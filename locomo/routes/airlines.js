﻿'use strict';
var express = require('express');
var rp = require('request-promise');
var router = express.Router();

router.get('/', function (req, res) {
    rp('http://node.locomote.com/code-task/airlines').then(function (body) {
        res.json(JSON.parse(body));
    }).catch(function (err) {
		console.log(err.message);
        res.status(500).json({ 'error': 'Communication error' });
    });
});

module.exports = router;