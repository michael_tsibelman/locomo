﻿'use strict';
var express = require('express');
var rp = require('request-promise');

var router = express.Router();

router.get('/', function (req, res) {
        
    rp('http://node.locomote.com/code-task/airports?q=' + req.query.q).then(function (body) {

        var retJSON = JSON.parse(body).map(function (obj) {
            return  {
                text: obj.cityName + ' - ' + obj.airportName + ' (' + obj.airportCode + ')',
                airportCode: obj.airportCode
            };            
        });

        res.json(retJSON);
    }).catch(function (err) {
        console.log(err.message);
        res.status(500).json({ 'error': 'Communication error' });
    });

});

module.exports = router;
