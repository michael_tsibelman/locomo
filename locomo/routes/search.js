﻿'use strict';

var express = require('express');
var console = require('console');
var rp = require('request-promise');

var promise = require('promise');
var moment = require('moment');
var util = require('util');
var router = express.Router();

function getUrls(params,airlineCode)
{
	var urlTemplate = 'http://node.locomote.com/code-task/flight_search/%s?date=%s&from=' + params.from.toUpperCase() + '&to=' + params.to.toUpperCase();
	var now = moment();
	var queryDate = moment(req.query.date, "YYYY-MM-DD").subtract(2, 'days');
	var urls = [];
	for (var i = 0; i < 5; i++){
		if (queryDate.isAfter(now, "day") || queryDate.isSame(now,"day")) {
			urls.push(util.format(urlTemplate, airlineCode, queryDate.format("YYYY-MM-DD")));
		}
		queryDate.add(1, 'days');
	}
	
	return urls;
}

function unifay(rawResults)
{
	 var results = rawResults.reduce(function (p, c) {
									   for (var i = 0; i < p.length; i++) {
										   p[i] = p[i].concat(c[i])
									   }
									   return p;
								     });

	var validResults = [];

	results.forEach(function (result) {
	   if (result.length > 0)
	   {
		   validResults.push({ date: moment(result[0].takeOf, 'YYYY-MM-DD').format('YYYY-MM-DD'), flights: result });
	   }                       
	});
    
    return validResults;                   
}

function flaten(airlineResults){
	var results = JSON.parse(airlineResults).map(function (res) {
		return {
			airlineCode: res.airline.code,
			airline: res.airline.name,
			durationMin: res.durationMin,
			flightNum: res.flightNum,
			takeOf: res.start.dateTime,
			landing: res.finish.dateTime,
			plane: res.plane.shortName,
			price: res.price
		}
	});
}

function HandaleError(err) {                        
	console.log(err.message);
	return promise.resolve([]);
}

router.get('/', function (req, res) {
  
    rp('http://node.locomote.com/code-task/airlines').then(function (body) {

        var airlines = JSON.parse(body);

        return promise
               .all(airlines.map(function (airline) {                  
					
					var urls = getUrls(req.query, airline.code);
					var reqPromises = urls.map(function (url) {
                        return  rp(url).then(function (airlineResults) {										
									var flatResults=flaten(airlineResults);								
									return promise.resolve(flatResults);																
								}).catch(HandaleError)
                    });
					
                    return promise.all(reqPromises);            
                }))
               .done(function(rawResults) {
				   var validResults = unifay(rawResults);
				   res.json(validResults);
			   }, HandaleError);
    })            
    .catch(function (err) {
        console.log(err.message);
        res.status(500).json({ 'error': 'Communication error' });
    })        
});

module.exports = router;