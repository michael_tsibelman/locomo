﻿$(document).ready(function () {

    $('#date').datepicker({
        autoHide: true,
        format: 'YYYY-MM-DD',
        startDate:new Date()
    });

    $('#date').val(moment().format('YYYY-MM-DD'));
    var fromAutoComplete = setupAutoComplete("#from");
    var toAutoComplete = setupAutoComplete("#to");
    
    function setupAutoComplete(name)
    {
        var autoComplete = new Awesomplete($(name)[0]);
        autoComplete.list = [];
        autoComplete.data = function (item, input) {
            return { label: item.text, value: item.airportCode };
        };
        var timerid;
        
        $(name).on("keyup", { query: "" }, function (event) {            
            if (event.which == 13)
                return;

            clearTimeout(timerid);
            var query = $(event.target).val();
            if (query.length > 1 && query != event.data.query) {
                timerid = setTimeout(function () {
                    $.ajax({
                        url: "/airports?q=" + query,
                        success: function (data) {
                            event.data.query = query;
                            autoComplete.list = data;                            
                        },
                        error: function () {
                            
                        }
                    });
                }, 400);                
            }
        });

        return autoComplete;
    }
         
    

      
    $("#search-button").on("click", function () {
        $(".container").css('display', 'none');
        $(".waiting").css('display', 'block');
        $(".fail").css('display', 'none');

        $.ajax({
            url: "/search/?date=" + $('#date').val() + "&from=" + $('#from').val() + "&to=" + $('#to').val(),
            success: function (results) {

                $('.container').empty();
                $('.container').append("<ul class='tabs'></ul>");                
                $(".container").css('display', 'block');
				
				var selectedTab="tab-1";
				
                $.each(results, function (tabIndex, tabResults) {

                    tabIndex++;
					
                    $('ul.tabs').append('<li class="tab-link" data-tab="tab-' + tabIndex + '">' + tabResults.date + '</li>');
                    $('.container').append('<div id="tab-' + tabIndex + '" class="tab-content"></div>')                        

                    if (tabResults.flights.length == 0) {
                        $('#tab-' + tabIndex).append("<h1>No results for this date.</h1>");
                    }  

                    $.each(tabResults.flights, function (index, item) {
                        
                        var html = "<div class='result'>"
                                + "<div>Flight number " + item.flightNum + " by " + item.airline + " price <span class='price'>" + item.price + "</span></div>"
                                + "<div>Take of at " + moment(item.takeOf).format("HH:mm") + " flight duration " + item.durationMin + " minutes</div>"
                                + "<div>Landing at " + moment(item.landing).format("YYYY-MM-DD HH:mm") + "</div>"
                                + "</div>";

                        $('#tab-' + tabIndex).append(html);
                    })   

					if(tabResults.date==$('#date').val())					
					{
						selectedTab="tab-"+tabIndex;
					}
                });
                
                if (results.length == 0) {
                    $(".fail").css('display', 'block');
                    $(".container").css('display', 'none');
                } else {
                    setActiveTab(selectedTab);
                    $('ul.tabs li').click(function () {
                        var tab_id = $(this).attr('data-tab');
                        setActiveTab(tab_id);
                    });
                }
                                
                $(".waiting").css('display', 'none');
            }
        });
    });

    function setActiveTab(tab_id) {
        
        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $('[data-tab="' + tab_id + '"]').addClass('current');
        $("#" + tab_id).addClass('current');
    }

}); 
